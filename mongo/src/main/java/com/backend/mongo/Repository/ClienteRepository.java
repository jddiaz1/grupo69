package com.backend.mongo.Repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.backend.mongo.Entity.Cliente;

@Repository
public interface ClienteRepository  extends MongoRepository<Cliente,String>{
    
    @Query("{emailCliente:?0,nombreCliente:?1}")
    public List<Cliente> findByCorreoNombre(String email,String nombre );

    
}
