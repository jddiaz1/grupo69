package com.backend.mongo.Service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.backend.mongo.Entity.Transaccion;
import com.backend.mongo.Repository.TransaccionRepository;

@Service
public class TransaccionService {
    @Autowired
    TransaccionRepository transaccionRepository;

    public Transaccion save(Transaccion transaccion){
        return transaccionRepository.save(transaccion);
    }

    public List<Transaccion> findAll(){
        return transaccionRepository.findAll();
    }

    public Transaccion findById(String id){
        return transaccionRepository.findById(id).get();
    }

    public String deleteById(String id){
        transaccionRepository.deleteById(id);
        return "Registro eliminado";
    }

    public List<Transaccion> buscarCuenta(String id){
        return transaccionRepository.consultarTransaccionesPorCuenta(id);
    }

    public List<Transaccion> buscarFecha(Date finicial,Date ffinal){
        return transaccionRepository.consultarFecha(finicial, ffinal);
    }
}
