package com.backend.mongo.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.backend.mongo.Entity.Cuenta;
import com.backend.mongo.Repository.CuentaRepository;

@Service
public class CuentaService {
    @Autowired
    CuentaRepository cuentaRepository;

    public Cuenta save(Cuenta cuenta){
        return cuentaRepository.save(cuenta);
    }

    public String deleteById(String id){
        cuentaRepository.deleteById(id);
        return "registro eliminado";
    }

    public Cuenta findById(String id){
        return cuentaRepository.findById(id).get();
    }

    public List<Cuenta> findAll(){
        return cuentaRepository.findAll();
    }

    
    public List<Cuenta> findByUser(String id){
        return cuentaRepository.buscarPorCliente(id);
    }
}
