package com.backend.mongo.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.backend.mongo.Entity.Cliente;
import com.backend.mongo.Repository.ClienteRepository;

@Service
public class ClienteService {
    @Autowired
    ClienteRepository clienteRepository;

    public Cliente save(Cliente cliente) {
        return clienteRepository.save(cliente);
    }

    public String deleteBy(String id) {
        clienteRepository.deleteById(id);
        return "registro eliminado";
    }

    public List<Cliente> findAll(){
        return clienteRepository.findAll();
    }

    public List<Cliente> findCorreoNombre(String correo, String nombre){
        return clienteRepository.findByCorreoNombre(correo, nombre);
    }

}
