package com.backend.mongo.Entity;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Document(value = "cuentas")
@Data
public class Cuenta {
    @Id
    private String idCuenta;
    private Date fechaCreacion;
    private double saldoCuenta;
    @DBRef
    private Cliente cliente;

}
