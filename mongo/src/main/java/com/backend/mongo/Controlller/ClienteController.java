package com.backend.mongo.Controlller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.backend.mongo.Entity.Cliente;
import com.backend.mongo.Service.ClienteService;

@RestController
@RequestMapping("/api/v1/cliente")
public class ClienteController {
    
    @Autowired
    ClienteService clienteService;

    @PostMapping("/create")
    public Cliente save(@RequestBody Cliente cliente){
        return clienteService.save(cliente);
    }

    @GetMapping("/list")
    public List<Cliente> findAll()
    {
        return clienteService.findAll();
    }

    @GetMapping("/correonombre/{correo}/{nombre}")
    public List<Cliente> findCorreoNombre(@PathVariable("correo") String correo,@PathVariable("nombre") String nombre)
    {
        return clienteService.findCorreoNombre(correo,nombre);
    }

    @DeleteMapping("/delete/{id}")
    public String deleteById(@PathVariable("id") String id){
        return clienteService.deleteBy(id);
    }

    
    @PutMapping("/update/{id}")
    public Cliente update(@RequestBody Cliente cliente,@PathVariable("id") String id){
        cliente.setIdCliente(id);
        return clienteService.save(cliente);
    }
}
