package com.crud.demo.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crud.demo.Entity.Demo;
import com.crud.demo.Repository.DemoRepository;

@Service
public class DemoService {
    @Autowired
    DemoRepository demoRepository;

    public Demo save(Demo demo){
        return demoRepository.save(demo);
    }
}
