package com.crud.demo.JWT.Repositorys;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.crud.demo.JWT.Entitys.ERole;
import com.crud.demo.JWT.Entitys.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role,Integer> {
    public Optional<Role> findByName(ERole name);
}
