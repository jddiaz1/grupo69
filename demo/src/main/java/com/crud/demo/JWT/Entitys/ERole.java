package com.crud.demo.JWT.Entitys;

public enum ERole {
    ROLE_USER,
    ROLE_ADMIN,
    ROLE_MODERATOR,
    ROLE_OTRO,
    ROLE_CAJERO,
}
